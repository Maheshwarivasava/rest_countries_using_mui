import React from 'react';
import './App.css';
import Home from './components/Home.jsx';
import { Routes, Route } from 'react-router-dom';
import CountryDetails from './components/CountryDetails/CountryDetails.jsx';
import Header from './components/Header/Header.jsx';
import { Box } from '@mui/material';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { useTheme } from './components/ThemeContext.jsx';
function App() {

  const { darkMode, toggleDarkMode } = useTheme();

  const theme = createTheme({
    palette: {
      mode: darkMode ? 'dark' : 'light',
      primary: {
        main: '#fff',
      },
      background: {
        paper: darkMode ? "hsl(209, 23%, 22%)" : "white",
        default: darkMode ? 'hsl(207, 26%, 17%)' : '#fff',
      },

    }
  })
  return (
    <ThemeProvider theme={theme}>
      <Box>
        <Header />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/country/:id" element={<CountryDetails />} />
        </Routes>
      </Box>
    </ThemeProvider>
  )
}

export default App