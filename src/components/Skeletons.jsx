import React from 'react';
import Skeleton from '@mui/material/Skeleton';

const Skeletons = ({ loop }) => {
  return (
    <>
      {Array(loop)
        .fill(0)
        .map((_, i) => {
          return (
            <div key={i} className="skeleton">
              <Skeleton animation="wave" variant="rectangular" height={"50%"} style={{ marginBottom: "0.5rem" }} />
              <div className="content-skeleton">
                <Skeleton animation="wave" variant="text" count={4} height={"1rem"} width={"100%"} style={{ marginBottom: "0.5rem" }} />
              </div>
            </div>
          );
        })}
    </>
  );
};

export default Skeletons;
