import React from 'react';
import { Card, CardContent, Typography, List, ListItem, ListItemText, ListItemIcon, Divider } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '../ThemeContext.jsx';

const CountryCard = ({ country, id }) => {
  const { darkMode } = useTheme();
  const navigate = useNavigate();

  const handleNavigate = () => {
    navigate(`/country/${id}`);
  };

  return (
    <Card onClick={handleNavigate} sx={{ bgcolor: darkMode && 'hsl(209, 23%, 22%)'}} style={{width: "100%", height: "100%"}}>

      <img src={country.flags.png} alt={country.name.common} style={{width: "100%", height: "50%"}}/>
      <CardContent>
        <Typography variant="h6" component="h2" sx={{ fontWeight: "900", overflow: "hidden" }}>
          {country.name.common}
        </Typography>
        <List sx={{  fontWeight: "500",color: darkMode ? 'white' :"hsl(200, 15%, 8%)"}}>
          <ListItem disablePadding >
            <ListItemIcon>
              <Typography sx={{fontWeight: "800" ,color: darkMode ? 'white':"hsl(200, 15%, 8%)"}}>Population:</Typography>
            </ListItemIcon>
            <ListItemText primary={country.population} />
          </ListItem>
          <ListItem disablePadding>
            <ListItemIcon>
              <Typography sx={{fontWeight: "800" ,color: darkMode ? 'white':"hsl(200, 15%, 8%)"}} variant="body2">Region:</Typography>
            </ListItemIcon>
            <ListItemText primary={country.region} />
          </ListItem>
          <ListItem disablePadding>
            <ListItemIcon>
              <Typography sx={{fontWeight: "800" ,color: darkMode ?'white':"hsl(200, 15%, 8%)"}} variant="body2">Capital:</Typography>
            </ListItemIcon>
            <ListItemText primary={country.capital ? country.capital[0] : "No Capital"} />
          </ListItem>
        </List>
      </CardContent>
    </Card>
  );
};

export default CountryCard;
