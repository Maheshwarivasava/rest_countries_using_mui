import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { Button, Typography } from '@mui/material'; 
import { useTheme } from '../ThemeContext.jsx';

const DarkModeContext = React.createContext();

const CountryDetails = () => {
    const [country, setCountry] = useState();
    const { id } = useParams();
    const navigate = useNavigate();
    const { darkMode } = useTheme();

    useEffect(() => {
        const fetchCountry = async () => {
            try {
                const response = await fetch(`https://restcountries.com/v3.1/alpha/${id}`);
                if (!response.ok) {
                    throw new Error('Failed to fetch country data');
                }
                const data = await response.json();
                setCountry(data[0]);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchCountry();
    }, [id]);

    return (
        <div className={`wrapper ${darkMode ? 'dark-mode' : ''}`}>
            <Button onClick={() => navigate(-1)} variant="outlined" color="primary">
                <FontAwesomeIcon icon={faArrowLeft} /> Back
            </Button>
            {country && (
                <div className="country-details">
                    <div className='flag'>
                        <img src={country.flags.svg} alt={`Flag of ${country.name.common}`} />
                    </div>
                    <div className='textData'>
                        <div className='country-info'>
                            <Typography variant="h4">{country.name.common}</Typography>
                            <div className='detail-all'>
                                <div className='detail-part1'>
                                    <Typography variant="body1"><span>Native Name:</span> {Object.values(country?.name?.nativeName)[0].common}</Typography>
                                    <Typography variant="body1"><span>Population:</span> {country?.population}</Typography>
                                    <Typography variant="body1"><span>Region:</span> {country?.region}</Typography>
                                    <Typography variant="body1"><span>Sub Region:</span> {country.subregion ? country.subregion[0] : "No subregion"}</Typography>
                                    <Typography variant="body1"><span>Capital:</span> {country.capital ? country.capital[0] : "No Capital"}</Typography>
                                </div>
                                <div className='detail-part2'>
                                    <Typography variant="body1"><span>Top Level Domain:</span> {country.tld ? country.tld[0] : "No Top Level Domain"}</Typography>
                                    <Typography variant="body1"><span>Currencies:</span> {country?.currency}</Typography>
                                    <Typography variant="body1"><span>Languages: </span>{country.languages ? Object.values(country.languages).join(",") : "No Languages"}</Typography>
                                </div>
                            </div>
                            <div className='border'>
                                <Typography variant="body1"><span>Border Countries: </span></Typography>
                                {country?.borders ? (
                                    country?.borders.map((country) => {
                                        return (
                                            <Button key={country?.name?.common} variant="outlined" className={darkMode ? "btn dark-mode" : "btn"}>
                                                {country}
                                            </Button>
                                        );
                                    })
                                ) : (
                                    <Typography variant="body1">No Border Countries</Typography>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div >
    );
}

export default CountryDetails;
