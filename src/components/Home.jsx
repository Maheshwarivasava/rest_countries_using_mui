import React, { useState, useEffect } from 'react';
import Filters from './Filters/Filters.jsx';
import { useTheme } from './ThemeContext.jsx';
import datanotfoundimage from '../assets/image2.svg';
import apifetcherrorimage from '../assets/error1.svg';
import { Grid, Typography } from '@mui/material';
import Skeleton from '@mui/material/Skeleton';
import CountryCard from './CountryCard/CountryCard.jsx';
import { Box } from '@mui/system';

const Home = () => {
  const { darkMode } = useTheme();
  const [countries, setCountries] = useState([]);
  const [searchInput, setSearchInput] = useState('');
  const [selectedRegion, setSelectedRegion] = useState('');
  const [selectedSubregion, setSelectedSubregion] = useState('');
  const [sortBy, setSortBy] = useState('population');
  const [sortOrder, setSortOrder] = useState('asc');
  const [error, setError] = useState(false);
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    async function fetchData() {
      try {
        let response = await fetch("https://restcountries.com/v3.1/all");
        if (!response.ok) {
          throw new Error("Failed to fetch data");
        }
        let data = await response.json();
        setCountries(data);
        setLoader(false);
      } catch (error) {
        setError(true);
      }
    }
    fetchData();
  }, []);

  let filteredCountries = countries.filter(country => {
    return (
      country.name.common.toLowerCase().includes(searchInput.toLowerCase()) &&
      (selectedRegion === '' || country.region.toLowerCase() === selectedRegion.toLowerCase()) &&
      (selectedSubregion === '' || country.subregion.toLowerCase() === selectedSubregion.toLowerCase())
    );
  });

  const sortCountries = () => {
    const sorted = [...filteredCountries].sort((a, b) => {
      if (sortBy === 'population') {
        return sortOrder === 'asc' ? a.population - b.population : b.population - a.population;
      } else if (sortBy === 'area') {
        return sortOrder === 'asc' ? a.area - b.area : b.area - a.area;
      }
      return 0;
    });
    return sorted;
  };

  const sortedCountries = sortCountries();

  return (
    <div className={`wrapper ${darkMode ? 'dark-mode' : ''}`}>
      <Box sx={{ bgcolor: darkMode && 'hsl(207, 26%, 17%)' }}>
        <Filters
          searchInput={searchInput}
          setSearchInput={setSearchInput}
          selectedRegion={selectedRegion}
          setSelectedRegion={setSelectedRegion}
          selectedSubregion={selectedSubregion}
          setSelectedSubregion={setSelectedSubregion}
          sortBy={sortBy}
          setSortBy={setSortBy}
          sortOrder={sortOrder}
          setSortOrder={setSortOrder}
          countries={countries}
        />
        {error && (
          <div className='not-found'>
            <img src={apifetcherrorimage} alt="not found" />
          </div>
        )}
        {loader && (
          <Grid container spacing={"4rem"}  sx={{ justifyContent: "center", padding: "4rem" }}>
            {[...Array(8)].map((_, index) => (
              <Grid item xs={12} sm={6} md={4} lg={3} key={index}>
                <Skeleton animation="wave" variant="rectangular" height={400} />
              </Grid>
            ))}
          </Grid>
        )}
        {!error && filteredCountries.length === 0 && (
          <div className='not-found'>
            <img src={datanotfoundimage} alt="not found" />
          </div>
        )}
        <Grid container spacing={"4rem"}  sx={{ justifyContent: "center", padding: {xs:"2rem",lg:"4rem"}}}>
          {sortedCountries.map(country => (
            <Grid item xs={12} sm={6} md={4} lg={3} key={country?.cca3} >
              <CountryCard country={country} id={country?.cca3} />
            </Grid>
          ))}
        </Grid>
      </Box>
    </div>

  );
}

export default Home;
