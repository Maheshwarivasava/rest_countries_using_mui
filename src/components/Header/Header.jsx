import React from 'react';
import { useTheme } from '../ThemeContext.jsx';
import { Button, Stack, Typography } from '@mui/material';
import DarkModeIcon from '@mui/icons-material/DarkMode';


const Header = () => {
  const { darkMode, toggleDarkMode } = useTheme();

  return (
    
      <header className={darkMode ? 'dark-mode' : ''}>
        <Stack direction="row" justifyContent="space-between"  sx={{ color: darkMode && 'white', bgcolor: darkMode && 'hsl(209, 23%, 22%)',padding:{xs:'1rem',lg:'4rem'} }}>
          <Typography variant="h4" component="h1" sx={{ fontWeight: 900,fontSize:{xs:"1.5rem"}}}>
            Where in the world?
          </Typography>
          <Button onClick={toggleDarkMode} color="inherit" sx={{ fontWeight: 600 ,fontSize:{xs:"0.7rem"}}}>
            <DarkModeIcon />
            {darkMode ? 'Light Mode' : 'Dark Mode'}
          </Button>
        </Stack>
      </header>
    
  );
};

export default Header;
