import React from 'react';
import { TextField, FormControl, InputLabel, Select, MenuItem, Stack, Box, ThemeProvider, createTheme } from '@mui/material';
import { Search as SearchIcon } from '@mui/icons-material';
import { useTheme } from '../ThemeContext.jsx';

const Filters = ({ searchInput, setSearchInput, selectedRegion, setSelectedRegion, selectedSubregion, setSelectedSubregion, countries, sortBy, setSortBy, sortOrder, setSortOrder }) => {
  const { darkMode } = useTheme();

  const getUniqueRegions = () => {
    const regions = countries.map(country => country.region);
    const uniqueRegions = [...new Set(regions)];
    return uniqueRegions;
  };

  const getUniqueSubregions = () => {
    const subregions = countries
      .filter(country => country.region === selectedRegion)
      .map(country => country.subregion)
      .filter((subregion, index, self) => self.indexOf(subregion) === index);
    return subregions;
  };

  return (
    <Stack direction={{ xs: 'column', md: 'row', lg: 'row' }} justifyContent={"space-between"} padding={4} sx={{ color: "white" }}>
      <TextField
        sx={{ width: { xs: '100%', lg: '30%' }, boxShadow: '2', bgcolor: darkMode && 'hsl(209, 23%, 22%)', margin: { xs: '0 0 1rem 0' } }}
        variant="outlined"
        className="search-input"
        placeholder="Search for a country..."
        InputProps={{
          startAdornment: (
            <SearchIcon />
          ),
        }}
        value={searchInput}
        onChange={(e) => setSearchInput(e.target.value)}
      />
      <FormControl sx={{ width: { xs: '100%', lg: '10%' }, boxShadow: '2', bgcolor: darkMode && 'hsl(209, 23%, 22%)', margin: { xs: '0 0 1rem 0'  } }}>
        <InputLabel id="region-select-label" >Region</InputLabel>
        <Select
          labelId="region-select-label"
          id="region-select"
          value={selectedRegion}
          label="Region"
          onChange={(e) => setSelectedRegion(e.target.value)}
        // variant='outlined'
        >
          <MenuItem value="">All Regions</MenuItem>
          {getUniqueRegions().map((region, index) => (
            <MenuItem key={index} value={region}>{region}</MenuItem>
          ))}
        </Select>
      </FormControl>
      {selectedRegion && (
        <FormControl sx={{ width: { xs: '100%', lg: '10%' }, boxShadow: '2', bgcolor: darkMode && 'hsl(209, 23%, 22%)', margin: { xs: '0 0 1rem 0'  } }}>
          <InputLabel id="subregion-select-label" >Subregion</InputLabel>
          <Select
            labelId="subregion-select-label"
            id="subregion-select"
            value={selectedSubregion}
            label="Subregion"
            onChange={(e) => setSelectedSubregion(e.target.value)}
          >
            <MenuItem value="">All Subregions</MenuItem>
            {getUniqueSubregions().map((subregion, index) => (
              <MenuItem key={index} value={subregion}>{subregion}</MenuItem>
            ))}
          </Select>
        </FormControl>
      )}
      <FormControl sx={{ width: { xs: '100%', lg: '10%' }, boxShadow: '2', bgcolor: darkMode && 'hsl(209, 23%, 22%)', margin: { xs: '0 0 1rem 0'  } }}>
        <InputLabel id="sort-by-select-label" >Sort by</InputLabel>
        <Select
          labelId="sort-by-select-label"
          id="sort-by-select"
          value={sortBy}
          label="Sort By"
          onChange={(e) => setSortBy(e.target.value)}
        >
          <MenuItem value="population">Population</MenuItem>
          <MenuItem value="area">Area</MenuItem>
        </Select>
      </FormControl>
      <FormControl sx={{ width: { xs: '100%', lg: '10%' }, boxShadow: '2', bgcolor: darkMode && 'hsl(209, 23%, 22%)', margin: { xs: '0 0 1rem 0'  } }}>
        <InputLabel id="sort-order-select-label"  >Sort order</InputLabel>
        <Select
          labelId="sort-order-select-label"
          id="sort-order-select"
          value={sortOrder}
          label="Sort Order"
          onChange={(e) => setSortOrder(e.target.value)}
        >
          <MenuItem value="asc">Ascending</MenuItem>
          <MenuItem value="desc">Descending</MenuItem>
        </Select>
      </FormControl>
    </Stack>

  );
};

export default Filters;
